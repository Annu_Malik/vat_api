package com.vatapi.steps;

import com.vatapi.actions.VatNumberActions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

public class VatNumberSteps {

    @Steps
    VatNumberActions vatNumberActions = new VatNumberActions();

    @When("the user send a GET request for vatnumber with (.*)$")
    public void theUserSendAGETRequestForVatnumberWithVatid(String vatid) throws Exception {
        vatNumberActions.requestVatnumberWithGetMethod(vatid);
    }

    @Then("^the user verifies content-type as JSON and status code as (.*)$")
    public void theUserVerifiesContentTypeAndStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", vatNumberActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, vatNumberActions.getStatusCode());
    }

    @When("the user send a GET request for vatid and with invalid apikey (.*) (.*)$")
    public void theUserSendAGetRequestForVatidWithInvalidapikey(String vatid, String apikey) throws Exception {
        vatNumberActions.requestSentForVatnumberWithInvalidApiKey(vatid, apikey);
    }

    @Then("the user sees valid status is true")
    public void theUserSeesValidStatusIsTrue() throws Exception {
        restAssuredThat(response -> response
                .body("validation.valid", equalTo(true)));
    }
}
