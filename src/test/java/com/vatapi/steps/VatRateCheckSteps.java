package com.vatapi.steps;

import com.vatapi.actions.VatRateCheckActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VatRateCheckSteps {
    @Steps
    VatRateCheckActions vatRateCheckActions = new VatRateCheckActions();

    @When("^user calls endpoint with rate type (.*) and country code (.*)$")
    public void userCallsEndpointWithRateTypeAndCountryCode(String RateType, String CountryCode) throws Exception {
        vatRateCheckActions.requestVatRateCheckWithGetMethod(RateType, CountryCode);
    }

    @Then("^user verifies the status code (.*)$")
    public void userVerifiesTheStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", vatRateCheckActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, vatRateCheckActions.getStatusCode());
    }

    @And("user verifies that it is a valid vatrate")
    public void userVerifiesThatItIsAValidVatrate() throws Exception {
        restAssuredThat(response -> response
                .body("vat_applies", equalTo(true)));
    }

    @When("^user call endpoint with rate type (.*) and country code (.*) with invalid apikey (.*)$")
    public void userCallEndpointWithRateTypeAndCountryCodeWithInvalidApikeyApikey(String RateType, String CountryCode, String apikey) throws Exception {
        vatRateCheckActions.requestSentForVatRateCheckWithInvalidApiKey(RateType, CountryCode, apikey);
    }
}
