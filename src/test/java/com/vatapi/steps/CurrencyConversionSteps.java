package com.vatapi.steps;

import com.vatapi.actions.CurrencyConversionActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

public class CurrencyConversionSteps {
    @Steps
    CurrencyConversionActions currencyConversionActions = new CurrencyConversionActions();

    @When("^the user send a GET request with currencyfrom (.*), amount (.*) and currencyto (.*)$")
    public void theUserSendAGETRequestWithCurrencyfromAmountAndCurrencyto(String currencyfrom, String Amount, String currencyto) throws Exception {
        currencyConversionActions.requestCurrencyConversionWithGetMethod(currencyfrom, Amount, currencyto);
    }

    @Then("^the user verifies status code as (.*) for currency conversion$")
    public void theUserVerifiesContentTypeAndStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", currencyConversionActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, currencyConversionActions.getStatusCode());
    }

    @And("Verify the fields as not null in response")
    public void verifyTheFieldsAsNotNullInResponse() throws Exception {
        assertNotNull(currencyConversionActions.getBody().getStatus());
        assertNotNull(currencyConversionActions.getBody().getCurrency_to());
        assertNotNull(currencyConversionActions.getBody().getAmount_from());
        assertNotNull(currencyConversionActions.getBody().getAmount_to());
        assertNotNull(currencyConversionActions.getBody().getRate());
    }

    @When("^the user send a GET request with currencyfrom (.*), amount (.*) currencyto (.*) with invalid apikey (.*)$")
    public void theUserSendAGETRequestWithInvalidApikeyApikey(String currencyfrom, String Amount, String currencyto, String apikey) throws Exception {
        currencyConversionActions.requestSentForCurrencyConversionWithInvalidApiKey(currencyfrom, Amount, currencyto, apikey);
    }
}
