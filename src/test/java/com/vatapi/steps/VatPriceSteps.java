package com.vatapi.steps;

import com.vatapi.actions.VatPriceActions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VatPriceSteps {
    VatPriceActions vatPriceActions = new VatPriceActions();

    @When("^user sends GET request with country code (.*), price (.*), rate type (.*), rate category (.*), rate level (.*) and vat charging method (.*)$")
    public void theUserSendsGETRequestWithCorrectValues(String CountryCode, String Price, String RateType, String RateCategory, String RateLevel, String VatChargingMethod) throws Exception {
        vatPriceActions.requestVatPriceWithGetMethod(CountryCode, Price, RateType, RateCategory, RateLevel, VatChargingMethod);
    }

    @Then("^user verifies status code as (.*)$")
    public void userVerifiesStatusCodeAsStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", vatPriceActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, vatPriceActions.getStatusCode());
    }

    @When("^user sends get request with country code (.*), price (.*), rate type (.*), rate category (.*), rate level (.*) and vat charging method (.*) with invalid (.*)$")
    public void requestVatPriceGetMethodWithInvalidApiKey(String CountryCode, String Price, String RateType, String RateCategory, String RateLevel, String VatChargingMethod, String apikey) throws Exception {
        vatPriceActions.requestSentForVatPriceWithInvalidApiKey(CountryCode, Price, RateType, RateCategory, RateLevel, VatChargingMethod, apikey);
    }
}
