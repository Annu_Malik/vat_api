package com.vatapi.steps;

import com.vatapi.actions.CreateRetrieveDeleteInvoiceActions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CreateRetrieveDeleteInvoiceSteps {
    @Steps
    CreateRetrieveDeleteInvoiceActions createRetrieveDeleteInvoiceActions = new CreateRetrieveDeleteInvoiceActions();

    @When("^user send POST request with settings identifier (.*), type (.*), currency (.*), name (.*), description (.*), quantity (.*), price each (.*) and vat rate (.*)$")
    public void userSendPOSTRequestWithAllFields(String SettingsIdentifier, String Type, String Currency, String Name, String Description, String Quantity, String PriceEach, String VatRate) throws Exception {
        createRetrieveDeleteInvoiceActions.requestSentForCreateInvoiceWithPostMethod(SettingsIdentifier, Type, Currency, Name, Description, Quantity, PriceEach, VatRate);
    }

    @Then("^User verify the status code as expected (.*)$")
    public void userVerifiesTheStatusCodeAsExpected(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", createRetrieveDeleteInvoiceActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, createRetrieveDeleteInvoiceActions.getStatusCode());
    }

    @When("^user sends a POST request with settings identifier (.*), type (.*), currency (.*), name (.*), description (.*), quantity (.*), price each (.*) and vat rate (.*) with invalid apikey (.*)$")
    public void userSendsAPOSTRequestWithWithInvalidApikey(String SettingsIdentifier, String Type, String Currency, String Name, String Description, String Quantity, String PriceEach, String VatRate, String apikey) throws Exception {
        createRetrieveDeleteInvoiceActions.requestSentForCreateInvoiceWithInvalidApiKey(SettingsIdentifier, Type, Currency, Name, Description, Quantity, PriceEach, VatRate, apikey);
    }

    @Then("User send a delete request for invoice")
    public void userSendADeleteRequestForInvoice() throws Exception {
        createRetrieveDeleteInvoiceActions.requestDeleteInvoiceWithDeleteMethod(createRetrieveDeleteInvoiceActions.getBody().getInvoice_id());
    }

    @Then("^User send a delete request with invalid invoice id (.*)$")
    public void userSendADeleteRequestWithInvalidInvoiceId(String InvoiceId) throws Exception {
        createRetrieveDeleteInvoiceActions.requestDeleteWithInvalidInvoiceId(InvoiceId);
    }

    @Then("^User sends a delete request for invoice with invalid apikey (.*)$")
    public void userSendsADeleteRequestForInvoiceWithInvalidApikeyApikey(String apikey) throws Exception {
        createRetrieveDeleteInvoiceActions.requestDeleteInvoiceWithInvalidApikey(apikey, createRetrieveDeleteInvoiceActions.getBody().getInvoice_id());
    }

    @Then("User send a get request for invoice")
    public void userSendAGetRequestForInvoice() throws Exception {
        createRetrieveDeleteInvoiceActions.requestGetInvoiceWithGetMethod(createRetrieveDeleteInvoiceActions.getBody().getInvoice_id());
    }

    @Then("^User send a get request with invalid invoice id (.*)$")
    public void userSendAGetRequestWithInvalidInvoiceIdInvoiceId(String InvoiceId) throws Exception {
        createRetrieveDeleteInvoiceActions.requestGetInvoiceWithInvalidInvoiceId(InvoiceId);
    }
}
