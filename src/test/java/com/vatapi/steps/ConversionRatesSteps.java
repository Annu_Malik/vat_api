package com.vatapi.steps;

import com.vatapi.actions.ConversionRatesActions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConversionRatesSteps {

    @Steps
    ConversionRatesActions conversionRatesActions = new ConversionRatesActions();

    @When("^user sends an api request for conversion rates with currency code (.*)$")
    public void userSendsAnApiRequestForConversionRatesWithCountryCode(String CurrencyCode) throws Exception {
        conversionRatesActions.requestConversionRatesWithGetMethod(CurrencyCode);
    }

    @Then("^user Compares the actual status code with expected (.*)$")
    public void userComparesTheActualStatusCodeWithExpectedStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", conversionRatesActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, conversionRatesActions.getStatusCode());
    }

    @When("^user sends get request for conversion rates with currency code (.*) and invalid apikey (.*)$")
    public void userSendsRequestForConversionRatesWithCurrencyCodeCurrencyCodeAndInvalidApikey(String CurrencyCode, String apikey) throws Exception {
        conversionRatesActions.requestSentForCurrencyConversionWithInvalidApiKey(CurrencyCode, apikey);
    }
}
