package com.vatapi.steps;

import com.vatapi.actions.VatRatesActions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VatRatesSteps {
    @Steps
    VatRatesActions vatRatesActions = new VatRatesActions();

    @When("^user send get request for VAT rates with rate type (.*)$")
    public void userSendGetRequestForVATRatesWithRateType(String RateType) throws Exception {
        vatRatesActions.requestVatRatesWithGetMethod(RateType);
    }

    @Then("^^User receives status code as expected (.*)$")
    public void userReceivesStatusCodeAsExpectedStatusCode(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", vatRatesActions.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, vatRatesActions.getStatusCode());
    }

    @When("^the user send get request for VAT rates with rate type (.*) and invalid apikey (.*)$")
    public void theUserSendRequestForVATRatesWithRateTypeRateTypeAndInvalidApikeyApikey(String RateType, String apikey) throws Exception {
        vatRatesActions.requestSentForVatRatesWithInvalidApiKey(RateType, apikey);
    }
}
