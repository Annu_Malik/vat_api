/*
 * VAT API
 * A developer friendly API to help your business achieve VAT compliance
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.vatapi.models;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * RetrieveInvoiceArray
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2021-09-27T15:19:39.898831+02:00[Europe/Amsterdam]")
public class RetrieveInvoiceArray {
  public static final String SERIALIZED_NAME_BUSINESS_ADDRESS = "business_address";
  @SerializedName(SERIALIZED_NAME_BUSINESS_ADDRESS)
  private String businessAddress;

  public static final String SERIALIZED_NAME_BUSINESS_NAME = "business_name";
  @SerializedName(SERIALIZED_NAME_BUSINESS_NAME)
  private String businessName;

  public static final String SERIALIZED_NAME_CONVERSION_RATE = "conversion_rate";
  @SerializedName(SERIALIZED_NAME_CONVERSION_RATE)
  private Integer conversionRate;

  public static final String SERIALIZED_NAME_CURRENCY_CODE = "currency_code";
  @SerializedName(SERIALIZED_NAME_CURRENCY_CODE)
  private String currencyCode;

  public static final String SERIALIZED_NAME_CURRENCY_CODE_CONVERSION = "currency_code_conversion";
  @SerializedName(SERIALIZED_NAME_CURRENCY_CODE_CONVERSION)
  private String currencyCodeConversion;

  public static final String SERIALIZED_NAME_CUSTOMER_ADDRESS = "customer_address";
  @SerializedName(SERIALIZED_NAME_CUSTOMER_ADDRESS)
  private String customerAddress;

  public static final String SERIALIZED_NAME_CUSTOMER_NAME = "customer_name";
  @SerializedName(SERIALIZED_NAME_CUSTOMER_NAME)
  private String customerName;

  public static final String SERIALIZED_NAME_CUSTOMER_VAT_NUMBER = "customer_vat_number";
  @SerializedName(SERIALIZED_NAME_CUSTOMER_VAT_NUMBER)
  private String customerVatNumber;

  public static final String SERIALIZED_NAME_DATE = "date";
  @SerializedName(SERIALIZED_NAME_DATE)
  private String date;

  public static final String SERIALIZED_NAME_DISCOUNT_RATE = "discount_rate";
  @SerializedName(SERIALIZED_NAME_DISCOUNT_RATE)
  private Integer discountRate;

  public static final String SERIALIZED_NAME_DISCOUNT_TOTAL = "discount_total";
  @SerializedName(SERIALIZED_NAME_DISCOUNT_TOTAL)
  private Integer discountTotal;

  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private Integer id;

  public static final String SERIALIZED_NAME_INVOICE_URL = "invoice_url";
  @SerializedName(SERIALIZED_NAME_INVOICE_URL)
  private String invoiceUrl;

  public static final String SERIALIZED_NAME_ITEMS = "items";
  @SerializedName(SERIALIZED_NAME_ITEMS)
  private List<InvoiceItems> items = new ArrayList<InvoiceItems>();

  public static final String SERIALIZED_NAME_LOGO_URL = "logo_url";
  @SerializedName(SERIALIZED_NAME_LOGO_URL)
  private String logoUrl;

  public static final String SERIALIZED_NAME_NOTES = "notes";
  @SerializedName(SERIALIZED_NAME_NOTES)
  private String notes;

  public static final String SERIALIZED_NAME_PRICE_TYPE = "price_type";
  @SerializedName(SERIALIZED_NAME_PRICE_TYPE)
  private String priceType;

  public static final String SERIALIZED_NAME_SUBTOTAL = "subtotal";
  @SerializedName(SERIALIZED_NAME_SUBTOTAL)
  private Integer subtotal;

  public static final String SERIALIZED_NAME_TAX_POINT = "tax_point";
  @SerializedName(SERIALIZED_NAME_TAX_POINT)
  private String taxPoint;

  public static final String SERIALIZED_NAME_TOTAL = "total";
  @SerializedName(SERIALIZED_NAME_TOTAL)
  private Integer total;

  public static final String SERIALIZED_NAME_TYPE = "type";
  @SerializedName(SERIALIZED_NAME_TYPE)
  private String type;

  public static final String SERIALIZED_NAME_VAT_NUMBER = "vat_number";
  @SerializedName(SERIALIZED_NAME_VAT_NUMBER)
  private String vatNumber;

  public static final String SERIALIZED_NAME_VAT_TOTAL = "vat_total";
  @SerializedName(SERIALIZED_NAME_VAT_TOTAL)
  private Integer vatTotal;

  public static final String SERIALIZED_NAME_ZERO_RATED = "zero_rated";
  @SerializedName(SERIALIZED_NAME_ZERO_RATED)
  private String zeroRated;


  public RetrieveInvoiceArray businessAddress(String businessAddress) {
    
    this.businessAddress = businessAddress;
    return this;
  }

   /**
   * Your business address
   * @return businessAddress
  **/
  @ApiModelProperty(required = true, value = "Your business address")

  public String getBusinessAddress() {
    return businessAddress;
  }



  public void setBusinessAddress(String businessAddress) {
    this.businessAddress = businessAddress;
  }


  public RetrieveInvoiceArray businessName(String businessName) {
    
    this.businessName = businessName;
    return this;
  }

   /**
   * Your business name
   * @return businessName
  **/
  @ApiModelProperty(required = true, value = "Your business name")

  public String getBusinessName() {
    return businessName;
  }



  public void setBusinessName(String businessName) {
    this.businessName = businessName;
  }


  public RetrieveInvoiceArray conversionRate(Integer conversionRate) {
    
    this.conversionRate = conversionRate;
    return this;
  }

   /**
   * The rate of conversion at time of supply
   * @return conversionRate
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The rate of conversion at time of supply")

  public Integer getConversionRate() {
    return conversionRate;
  }



  public void setConversionRate(Integer conversionRate) {
    this.conversionRate = conversionRate;
  }


  public RetrieveInvoiceArray currencyCode(String currencyCode) {
    
    this.currencyCode = currencyCode;
    return this;
  }

   /**
   * 3 character currency code for invoice
   * @return currencyCode
  **/
  @ApiModelProperty(required = true, value = "3 character currency code for invoice")

  public String getCurrencyCode() {
    return currencyCode;
  }



  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }


  public RetrieveInvoiceArray currencyCodeConversion(String currencyCodeConversion) {
    
    this.currencyCodeConversion = currencyCodeConversion;
    return this;
  }

   /**
   * 3 character currency code to be converted from original transaction currency
   * @return currencyCodeConversion
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "3 character currency code to be converted from original transaction currency")

  public String getCurrencyCodeConversion() {
    return currencyCodeConversion;
  }



  public void setCurrencyCodeConversion(String currencyCodeConversion) {
    this.currencyCodeConversion = currencyCodeConversion;
  }


  public RetrieveInvoiceArray customerAddress(String customerAddress) {
    
    this.customerAddress = customerAddress;
    return this;
  }

   /**
   * Your customers address
   * @return customerAddress
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Your customers address")

  public String getCustomerAddress() {
    return customerAddress;
  }



  public void setCustomerAddress(String customerAddress) {
    this.customerAddress = customerAddress;
  }


  public RetrieveInvoiceArray customerName(String customerName) {
    
    this.customerName = customerName;
    return this;
  }

   /**
   * Your customers name or trading name
   * @return customerName
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Your customers name or trading name")

  public String getCustomerName() {
    return customerName;
  }



  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }


  public RetrieveInvoiceArray customerVatNumber(String customerVatNumber) {
    
    this.customerVatNumber = customerVatNumber;
    return this;
  }

   /**
   * Optional, customers VAT number
   * @return customerVatNumber
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Optional, customers VAT number")

  public String getCustomerVatNumber() {
    return customerVatNumber;
  }



  public void setCustomerVatNumber(String customerVatNumber) {
    this.customerVatNumber = customerVatNumber;
  }


  public RetrieveInvoiceArray date(String date) {
    
    this.date = date;
    return this;
  }

   /**
   * The date the invoice was issued
   * @return date
  **/
  @ApiModelProperty(required = true, value = "The date the invoice was issued")

  public String getDate() {
    return date;
  }



  public void setDate(String date) {
    this.date = date;
  }


  public RetrieveInvoiceArray discountRate(Integer discountRate) {
    
    this.discountRate = discountRate;
    return this;
  }

   /**
   * The discount rate per item
   * @return discountRate
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The discount rate per item")

  public Integer getDiscountRate() {
    return discountRate;
  }



  public void setDiscountRate(Integer discountRate) {
    this.discountRate = discountRate;
  }


  public RetrieveInvoiceArray discountTotal(Integer discountTotal) {
    
    this.discountTotal = discountTotal;
    return this;
  }

   /**
   * Total amount of discount
   * @return discountTotal
  **/
  @ApiModelProperty(required = true, value = "Total amount of discount")

  public Integer getDiscountTotal() {
    return discountTotal;
  }



  public void setDiscountTotal(Integer discountTotal) {
    this.discountTotal = discountTotal;
  }


  public RetrieveInvoiceArray id(Integer id) {
    
    this.id = id;
    return this;
  }

   /**
   * The invoice ID
   * @return id
  **/
  @ApiModelProperty(required = true, value = "The invoice ID")

  public Integer getId() {
    return id;
  }



  public void setId(Integer id) {
    this.id = id;
  }


  public RetrieveInvoiceArray invoiceUrl(String invoiceUrl) {
    
    this.invoiceUrl = invoiceUrl;
    return this;
  }

   /**
   * A perminant URL to your VAT invoice
   * @return invoiceUrl
  **/
  @ApiModelProperty(required = true, value = "A perminant URL to your VAT invoice")

  public String getInvoiceUrl() {
    return invoiceUrl;
  }



  public void setInvoiceUrl(String invoiceUrl) {
    this.invoiceUrl = invoiceUrl;
  }


  public RetrieveInvoiceArray items(List<InvoiceItems> items) {
    
    this.items = items;
    return this;
  }

  public RetrieveInvoiceArray addItemsItem(InvoiceItems itemsItem) {
    this.items.add(itemsItem);
    return this;
  }

   /**
   * An array of your invoice items
   * @return items
  **/
  @ApiModelProperty(required = true, value = "An array of your invoice items")

  public List<InvoiceItems> getItems() {
    return items;
  }



  public void setItems(List<InvoiceItems> items) {
    this.items = items;
  }


  public RetrieveInvoiceArray logoUrl(String logoUrl) {
    
    this.logoUrl = logoUrl;
    return this;
  }

   /**
   * A URL to your logo image. Must be SSL hosted. https://sslimagehost.com is recommended
   * @return logoUrl
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "A URL to your logo image. Must be SSL hosted. https://sslimagehost.com is recommended")

  public String getLogoUrl() {
    return logoUrl;
  }



  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }


  public RetrieveInvoiceArray notes(String notes) {
    
    this.notes = notes;
    return this;
  }

   /**
   * Any notes attached to the invoice
   * @return notes
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Any notes attached to the invoice")

  public String getNotes() {
    return notes;
  }



  public void setNotes(String notes) {
    this.notes = notes;
  }


  public RetrieveInvoiceArray priceType(String priceType) {
    
    this.priceType = priceType;
    return this;
  }

   /**
   * Optional, if the price is including VAT set the type to &#39;incl&#39;. Otherwise the default is assumed as excluding VAT already, &#39;excl&#39;
   * @return priceType
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Optional, if the price is including VAT set the type to 'incl'. Otherwise the default is assumed as excluding VAT already, 'excl'")

  public String getPriceType() {
    return priceType;
  }



  public void setPriceType(String priceType) {
    this.priceType = priceType;
  }


  public RetrieveInvoiceArray subtotal(Integer subtotal) {
    
    this.subtotal = subtotal;
    return this;
  }

   /**
   * Total amount excluding VAT
   * @return subtotal
  **/
  @ApiModelProperty(required = true, value = "Total amount excluding VAT")

  public Integer getSubtotal() {
    return subtotal;
  }



  public void setSubtotal(Integer subtotal) {
    this.subtotal = subtotal;
  }


  public RetrieveInvoiceArray taxPoint(String taxPoint) {
    
    this.taxPoint = taxPoint;
    return this;
  }

   /**
   * (or &#39;time of supply&#39;) if this is different from the invoice date
   * @return taxPoint
  **/
  @ApiModelProperty(required = true, value = "(or 'time of supply') if this is different from the invoice date")

  public String getTaxPoint() {
    return taxPoint;
  }



  public void setTaxPoint(String taxPoint) {
    this.taxPoint = taxPoint;
  }


  public RetrieveInvoiceArray total(Integer total) {
    
    this.total = total;
    return this;
  }

   /**
   * Total amount of including VAT
   * @return total
  **/
  @ApiModelProperty(required = true, value = "Total amount of including VAT")

  public Integer getTotal() {
    return total;
  }



  public void setTotal(Integer total) {
    this.total = total;
  }


  public RetrieveInvoiceArray type(String type) {
    
    this.type = type;
    return this;
  }

   /**
   * The type of invoice. Either &#39;sale&#39; or &#39;refund&#39;
   * @return type
  **/
  @ApiModelProperty(required = true, value = "The type of invoice. Either 'sale' or 'refund'")

  public String getType() {
    return type;
  }



  public void setType(String type) {
    this.type = type;
  }


  public RetrieveInvoiceArray vatNumber(String vatNumber) {
    
    this.vatNumber = vatNumber;
    return this;
  }

   /**
   * Your VAT number
   * @return vatNumber
  **/
  @ApiModelProperty(required = true, value = "Your VAT number")

  public String getVatNumber() {
    return vatNumber;
  }



  public void setVatNumber(String vatNumber) {
    this.vatNumber = vatNumber;
  }


  public RetrieveInvoiceArray vatTotal(Integer vatTotal) {
    
    this.vatTotal = vatTotal;
    return this;
  }

   /**
   * Total amount of VAT
   * @return vatTotal
  **/
  @ApiModelProperty(required = true, value = "Total amount of VAT")

  public Integer getVatTotal() {
    return vatTotal;
  }



  public void setVatTotal(Integer vatTotal) {
    this.vatTotal = vatTotal;
  }


  public RetrieveInvoiceArray zeroRated(String zeroRated) {
    
    this.zeroRated = zeroRated;
    return this;
  }

   /**
   * To Zero-Rate the VAT, set to true.
   * @return zeroRated
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "To Zero-Rate the VAT, set to true.")

  public String getZeroRated() {
    return zeroRated;
  }



  public void setZeroRated(String zeroRated) {
    this.zeroRated = zeroRated;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RetrieveInvoiceArray retrieveInvoiceArray = (RetrieveInvoiceArray) o;
    return Objects.equals(this.businessAddress, retrieveInvoiceArray.businessAddress) &&
        Objects.equals(this.businessName, retrieveInvoiceArray.businessName) &&
        Objects.equals(this.conversionRate, retrieveInvoiceArray.conversionRate) &&
        Objects.equals(this.currencyCode, retrieveInvoiceArray.currencyCode) &&
        Objects.equals(this.currencyCodeConversion, retrieveInvoiceArray.currencyCodeConversion) &&
        Objects.equals(this.customerAddress, retrieveInvoiceArray.customerAddress) &&
        Objects.equals(this.customerName, retrieveInvoiceArray.customerName) &&
        Objects.equals(this.customerVatNumber, retrieveInvoiceArray.customerVatNumber) &&
        Objects.equals(this.date, retrieveInvoiceArray.date) &&
        Objects.equals(this.discountRate, retrieveInvoiceArray.discountRate) &&
        Objects.equals(this.discountTotal, retrieveInvoiceArray.discountTotal) &&
        Objects.equals(this.id, retrieveInvoiceArray.id) &&
        Objects.equals(this.invoiceUrl, retrieveInvoiceArray.invoiceUrl) &&
        Objects.equals(this.items, retrieveInvoiceArray.items) &&
        Objects.equals(this.logoUrl, retrieveInvoiceArray.logoUrl) &&
        Objects.equals(this.notes, retrieveInvoiceArray.notes) &&
        Objects.equals(this.priceType, retrieveInvoiceArray.priceType) &&
        Objects.equals(this.subtotal, retrieveInvoiceArray.subtotal) &&
        Objects.equals(this.taxPoint, retrieveInvoiceArray.taxPoint) &&
        Objects.equals(this.total, retrieveInvoiceArray.total) &&
        Objects.equals(this.type, retrieveInvoiceArray.type) &&
        Objects.equals(this.vatNumber, retrieveInvoiceArray.vatNumber) &&
        Objects.equals(this.vatTotal, retrieveInvoiceArray.vatTotal) &&
        Objects.equals(this.zeroRated, retrieveInvoiceArray.zeroRated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(businessAddress, businessName, conversionRate, currencyCode, currencyCodeConversion, customerAddress, customerName, customerVatNumber, date, discountRate, discountTotal, id, invoiceUrl, items, logoUrl, notes, priceType, subtotal, taxPoint, total, type, vatNumber, vatTotal, zeroRated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RetrieveInvoiceArray {\n");
    sb.append("    businessAddress: ").append(toIndentedString(businessAddress)).append("\n");
    sb.append("    businessName: ").append(toIndentedString(businessName)).append("\n");
    sb.append("    conversionRate: ").append(toIndentedString(conversionRate)).append("\n");
    sb.append("    currencyCode: ").append(toIndentedString(currencyCode)).append("\n");
    sb.append("    currencyCodeConversion: ").append(toIndentedString(currencyCodeConversion)).append("\n");
    sb.append("    customerAddress: ").append(toIndentedString(customerAddress)).append("\n");
    sb.append("    customerName: ").append(toIndentedString(customerName)).append("\n");
    sb.append("    customerVatNumber: ").append(toIndentedString(customerVatNumber)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    discountRate: ").append(toIndentedString(discountRate)).append("\n");
    sb.append("    discountTotal: ").append(toIndentedString(discountTotal)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    invoiceUrl: ").append(toIndentedString(invoiceUrl)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("    logoUrl: ").append(toIndentedString(logoUrl)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    priceType: ").append(toIndentedString(priceType)).append("\n");
    sb.append("    subtotal: ").append(toIndentedString(subtotal)).append("\n");
    sb.append("    taxPoint: ").append(toIndentedString(taxPoint)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    vatNumber: ").append(toIndentedString(vatNumber)).append("\n");
    sb.append("    vatTotal: ").append(toIndentedString(vatTotal)).append("\n");
    sb.append("    zeroRated: ").append(toIndentedString(zeroRated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

