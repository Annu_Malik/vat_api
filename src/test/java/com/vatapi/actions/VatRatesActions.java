package com.vatapi.actions;

import com.vatapi.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class VatRatesActions {
    public Response response;

    @Step("Retrieve Url")
    public void requestVatRatesWithGetMethod(String Ratetype) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .param("rate_type", Ratetype)
                .get(Constants.BASE_URL + "/vat-rates");
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve request with invalid apikey")
    public void requestSentForVatRatesWithInvalidApiKey(String Ratetype, String apikey) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .param("rate_type", Ratetype)
                .get(Constants.BASE_URL + "/vat-rates");
    }
}
