package com.vatapi.actions;

import com.vatapi.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class VatRateCheckActions {
    public Response response;

    @Step("Retrieve Url")
    public void requestVatRateCheckWithGetMethod(String RateType, String CountryCode) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .get(Constants.BASE_URL + "/vat-rate-check" + "?rate_type=" + RateType + "&country_code=" + CountryCode);
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve request with invalid apikey")
    public void requestSentForVatRateCheckWithInvalidApiKey(String RateType, String CountryCode, String apikey) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .get(Constants.BASE_URL + "/vat-rate-check" + "?rate_type=" + RateType + "&country_code=" + CountryCode);
    }
}
