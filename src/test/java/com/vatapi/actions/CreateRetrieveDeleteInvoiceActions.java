package com.vatapi.actions;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vatapi.models.Customer;
import com.vatapi.models.InvoiceArray;
import com.vatapi.models.InvoiceItems;
import com.vatapi.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;

public class CreateRetrieveDeleteInvoiceActions {
    public Response response;

    @Step("Retrieve Url with post method")
    public void requestSentForCreateInvoiceWithPostMethod(String SettingsIdentifier, String Type, String Currency, String Name, String Description, String Quantity, String PriceEach, String VatRate) throws Exception {
        InvoiceArray invoiceArray = new InvoiceArray();
        invoiceArray.setSettings_identifier(SettingsIdentifier);
        invoiceArray.setType(Type);
        invoiceArray.setCurrency(Currency);
        Customer customer = new Customer();
        customer.setName(Name);
        invoiceArray.setCustomer(customer);
        InvoiceItems invoiceItems = new InvoiceItems();
        invoiceItems.setDescription(Description);
        invoiceItems.setPriceEach(Double.parseDouble(PriceEach));
        invoiceItems.setQuantity(Integer.parseInt(Quantity));
        invoiceItems.setVatRate(Integer.parseInt(VatRate));
        invoiceArray.setLine_items(asList(invoiceItems));

        ObjectMapper mapper = new ObjectMapper();
        String invoiceData = mapper.writeValueAsString(invoiceArray);

        //        System.out.println(invoiceData);

        response = SerenityRest.given().body(invoiceData)
                .header(("content-type"), "application/json")
                .header("Accept", ContentType.JSON.getAcceptHeader())
                .header("x-api-key", Constants.API_KEY)
                .post(Constants.BASE_URL + "/invoice");
    }

    @Step("Retrieve post request with invalid apikey")
    public void requestSentForCreateInvoiceWithInvalidApiKey(String SettingsIdentifier, String Type, String Currency, String Name, String Description, String Quantity, String PriceEach, String VatRate, String apikey) throws Exception {
        InvoiceArray invoiceArray = new InvoiceArray();
        invoiceArray.setSettings_identifier(SettingsIdentifier);
        invoiceArray.setType(Type);
        invoiceArray.setCurrency(Currency);
        Customer customer = new Customer();
        customer.setName(Name);
        invoiceArray.setCustomer(customer);
        InvoiceItems invoiceItems = new InvoiceItems();
        invoiceItems.setDescription(Description);
        invoiceItems.setPriceEach(Double.parseDouble(PriceEach));
        invoiceItems.setQuantity(Integer.parseInt(Quantity));
        invoiceItems.setVatRate(Integer.parseInt(VatRate));
        invoiceArray.setLine_items(asList(invoiceItems));

        ObjectMapper mapper = new ObjectMapper();
        String invoiceData = mapper.writeValueAsString(invoiceArray);

        //        System.out.println(invoiceData);

        response = SerenityRest.given().body(invoiceData)
                .header(("content-type"), "application/json")
                .header("Accept", ContentType.JSON.getAcceptHeader())
                .header("x-api-key", apikey)
                .post(Constants.BASE_URL + "/invoice");
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve delete method")
    public void requestDeleteInvoiceWithDeleteMethod(String invoice_id) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .delete(Constants.BASE_URL + "/invoice" + "/" + invoice_id);
    }

    @Step("Retrieve response body for invoice array")
    public InvoiceArray getBody() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(response.getBody().asByteArray(), InvoiceArray.class);
    }

    @Step("Retrieve delete request with invalid invoice number")
    public void requestDeleteWithInvalidInvoiceId(String InvoiceId) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .delete(Constants.BASE_URL + "/invoice" + "/" + InvoiceId);
    }

    @Step("Retrieve delete request with invalid apikey")
    public void requestDeleteInvoiceWithInvalidApikey(String apikey, String invoice_id) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .delete(Constants.BASE_URL + "/invoice" + "/" + invoice_id);
    }

    @Step("Retrieve get method")
    public void requestGetInvoiceWithGetMethod(String invoice_id) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .get(Constants.BASE_URL + "/invoice" + "/" + invoice_id);
    }

    @Step("Retrieve get request with invalid invoice number")
    public void requestGetInvoiceWithInvalidInvoiceId(String InvoiceId) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .get(Constants.BASE_URL + "/invoice" + "/" + InvoiceId);
    }
}
