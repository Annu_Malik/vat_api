package com.vatapi.actions;

import com.vatapi.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ConversionRatesActions {
    public Response response;

    @Step("Retrieve Url")
    public void requestConversionRatesWithGetMethod(String CurrencyCode) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .param("currency_code", CurrencyCode)
                .get(Constants.BASE_URL + "/currency-conversion-rates");
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve request with invalid apikey")
    public void requestSentForCurrencyConversionWithInvalidApiKey(String CurrencyCode, String apikey) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .param("currency_code", CurrencyCode)
                .get(Constants.BASE_URL + "/currency-conversion-rates");
    }
}
