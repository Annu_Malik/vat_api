package com.vatapi.actions;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vatapi.models.CurrencyConversion;
import com.vatapi.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class CurrencyConversionActions {
    public Response response;

    @Step("Retrieve Url")
    public void requestCurrencyConversionWithGetMethod(String currencyfrom, String Amount, String currencyto) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .get(Constants.BASE_URL + "/currency-conversion" + "?currency_from=" + currencyfrom + "&amount=" + Amount + "&currency_to=" + currencyto);
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve request with invalid apikey")
    public void requestSentForCurrencyConversionWithInvalidApiKey(String currencyfrom, String Amount, String currencyto, String apikey) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .get(Constants.BASE_URL + "/currency-conversion" + "?currency_from=" + currencyfrom + "&amount=" + Amount + "&currency_to=" + currencyto);
    }

    @Step("Retrieve response body")
    public CurrencyConversion getBody() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(response.getBody().asByteArray(), CurrencyConversion.class);
    }
}
