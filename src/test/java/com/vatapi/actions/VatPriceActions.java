package com.vatapi.actions;

import com.vatapi.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class VatPriceActions {
    public Response response;

    @Step("Retrieve Url")
    public void requestVatPriceWithGetMethod(String CountryCode, String Price, String RateType, String RateCategory, String RateLevel, String VatChargingMethod) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", Constants.API_KEY)
                .get(Constants.BASE_URL + "/vat-price" + "?country_code=" + CountryCode + "&price=" + Price + "&rate_type=" + RateType + "&rate_category=" + RateCategory + "&rate_level=" + RateLevel + "&vat_charging_method=" + VatChargingMethod);
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }

    @Step("Retrieve Url")
    public void requestSentForVatPriceWithInvalidApiKey(String CountryCode, String Price, String RateType, String RateCategory, String RateLevel, String VatChargingMethod, String apikey) throws Exception {
        response = SerenityRest.given()
                .header("x-api-key", apikey)
                .get(Constants.BASE_URL + "/vat-price" + "?country_code=" + CountryCode + "&price=" + Price + "&rate_type=" + RateType + "&rate_category=" + RateCategory + "&rate_level=" + RateLevel + "&vat_charging_method=" + VatChargingMethod);
    }
}
