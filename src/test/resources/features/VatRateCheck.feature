Feature: Validate the VAT rate check for specified country code and product type.

  @VATRateCheck-200
  Scenario Outline: Verifying VAT rate check for valid rate type and country code
    When user calls endpoint with rate type <RateType> and country code <CountryCode>
    Then user verifies the status code <StatusCode>
    And user verifies that it is a valid vatrate
    Examples:
      | RateType | CountryCode | StatusCode |
      | TBE      | NL          | 200        |
      | GOODS    | SE          | 200        |

  @VATRateCheck-400
  Scenario Outline: Verifying VAT rate check for invalid rate type and country code
    When user calls endpoint with rate type <RateType> and country code <CountryCode>
    Then user verifies the status code <StatusCode>
    Examples:
      | RateType | CountryCode | StatusCode |
      | TB       | NL          | 400        |
      | GOODS    | S1          | 400        |

  @VATRateCheck-403
  Scenario Outline: Verifying VAT rate check for invalid apikey
    When user call endpoint with rate type <RateType> and country code <CountryCode> with invalid apikey <apikey>
    Then user verifies the status code <StatusCode>
    Examples:
      | RateType | CountryCode | apikey  | StatusCode |
      | TB       | NL          | invalid | 403        |
      | GOODS    | S1          | invalid | 403        |
