Feature:Validate a VAT number

  @VATNumber-200
  Scenario Outline: : Verifying VAT number with vatid
    When the user send a GET request for vatnumber with <vatid>
    Then the user verifies content-type as JSON and status code as <StatusCode>
    Examples:
      | vatid  | StatusCode |
      | LU2637 | 200        |

  @VATNumber-400
  Scenario Outline: : Verifying VAT number with invalid vatid
    When the user send a GET request for vatnumber with <vatid>
    Then the user verifies content-type as JSON and status code as <StatusCode>
    Examples:
      | vatid             | StatusCode |
      | N L               | 400        |
      | bcnvbfh8457dbvu43 | 400        |
      | 5.6               | 400        |

  @VATNumber-403
  Scenario Outline: Verifying VAT number with invalid api key
    When the user send a GET request for vatid and with invalid apikey <vatid> <apikey>
    Then the user verifies content-type as JSON and status code as <StatusCode>
    Examples:
      | vatid      | apikey  | StatusCode |
      | LU26375245 | invalid | 403        |

  @VATNumber-200
  Scenario Outline: Verifying valid field is true when vat number is valid
    When the user send a GET request for vatnumber with <vatid>
    Then the user sees valid status is true
    Examples:
      | vatid      |
      | LU26375245 |
