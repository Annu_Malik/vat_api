Feature:Validate converting price to VAT price

  @VATPrice-200
  Scenario Outline: : Verifying VAT price with valid fields
    When user sends GET request with country code <CountryCode>, price <Price>, rate type <RateType>, rate category <RateCategory>, rate level <RateLevel> and vat charging method <VatChargingMethod>
    Then user verifies status code as <StatusCode>
    Examples:
      | CountryCode | Price | RateType | RateCategory | RateLevel | VatChargingMethod | StatusCode |
      | IT          | 9     | TBE      | electronic   | reduced_1 | inclusive         | 200        |

  @VATPrice-400
  Scenario Outline: : Verifying VAT price with invalid fields
    When user sends GET request with country code <CountryCode>, price <Price>, rate type <RateType>, rate category <RateCategory>, rate level <RateLevel> and vat charging method <VatChargingMethod>
    Then user verifies status code as <StatusCode>
    Examples:
      | CountryCode | Price | RateType | RateCategory | RateLevel | VatChargingMethod | StatusCode |
      | TI          | 9     | TBE      | electronic   | reduced_1 | inclusive         | 400        |

  @VATPrice-403
  Scenario Outline:Verifying VAT price with invalid apikey
    When user sends get request with country code <CountryCode>, price <Price>, rate type <RateType>, rate category <RateCategory>, rate level <RateLevel> and vat charging method <VatChargingMethod> with invalid <apikey>
    Then user verifies status code as <StatusCode>
    Examples:
      | CountryCode | Price | RateType | RateCategory | RateLevel | VatChargingMethod | apikey  | StatusCode |
      | IT          | 9     | TBE      | electronic   | reduced_1 | inclusive         | invalid | 403        |
