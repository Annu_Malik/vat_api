Feature: Validate retrieving the current and updated exchange rates

  @ConversionRates-200
  Scenario Outline: Verifying conversion rates with valid currency code
    When user sends an api request for conversion rates with currency code <CurrencyCode>
    Then user Compares the actual status code with expected <StatusCode>
    Examples:
      | CurrencyCode | StatusCode |
      | EUR          | 200        |
      | GBP          | 200        |

  @ConversionRates-400
  Scenario Outline: Verifying conversion rates with invalid currency code
    When user sends an api request for conversion rates with currency code <CurrencyCode>
    Then user Compares the actual status code with expected <StatusCode>
    Examples:
      | CurrencyCode | StatusCode |
      | EUT          | 400        |
      | GBR          | 400        |

  @ConversionRates-403
  Scenario Outline: Verifying conversion rates with invalid apikey
    When user sends get request for conversion rates with currency code <CurrencyCode> and invalid apikey <apikey>
    Then user Compares the actual status code with expected <StatusCode>
    Examples:
      | CurrencyCode | apikey  | StatusCode |
      | EUR          | invalid | 403        |
      | GBP          | invalid | 403        |
