Feature: Validate creating, retrieving and deleting invoice

  @CreateInvoice-201
  Scenario Outline: Verifying creating invoice and verifies status code with valid values
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 201        |

  @CreateInvoice-400
  Scenario Outline: Verifying creating invoice with invalid values
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sal  | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 400        |

  @CreateInvoice-403
  Scenario Outline: Verifying Creating invoice with invalid apikey
    When user sends a POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate> with invalid apikey <apikey>
    Then User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | apikey  | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | invalid | 403        |


  @DeleteInvoice-200
  Scenario Outline: Verifying Creating, retrieving and deleting invoice and verifies status code with valid values
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User send a delete request for invoice
    And User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 200        |

  @DeleteInvoice-404
  Scenario Outline: Verifying deleting invoice with invalid invoice id
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User send a delete request with invalid invoice id <InvoiceId>
    And User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode | InvoiceId |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 404        | null      |

  @DeleteInvoice-403
  Scenario Outline: Verifying deleting invoice with invalid apikey
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User sends a delete request for invoice with invalid apikey <apikey>
    And User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | apikey  | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | invalid | 403        |

  @RetrieveInvoice-200
  Scenario Outline: Verifying retrieving invoice and verifies status code with valid values
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User send a get request for invoice
    And User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 200        |

  @DeleteInvoice-404
  Scenario Outline: Verifying retrieving invoice with invalid invoice id
    When user send POST request with settings identifier <SettingsIdentifier>, type <Type>, currency <Currency>, name <Name>, description <Description>, quantity <Quantity>, price each <PriceEach> and vat rate <VatRate>
    Then User send a get request with invalid invoice id <InvoiceId>
    And User verify the status code as expected <StatusCode>
    Examples:
      | SettingsIdentifier               | Type | Currency | Name   | Description | Quantity | PriceEach | VatRate | StatusCode | InvoiceId |
      | 12a048d4094337e07efd0f1fd9fe6521 | sale | EUR      | Amazon | Product1    | 1        | 10.00     | 17      | 404        | null      |

