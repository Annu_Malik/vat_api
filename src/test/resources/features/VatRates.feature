Feature: Validate retrieving the current VAT rates for specified product type.

  @VATRates-200
  Scenario Outline: Verifying VAT rates with valid rate type
    When user send get request for VAT rates with rate type <RateType>
    Then User receives status code as expected <StatusCode>
    Examples:
      | RateType | StatusCode |
      | TBE      | 200        |
      | GOODS    | 200        |

  @VATRates-400
  Scenario Outline: Verifying VAT rates with invalid rate type
    When user send get request for VAT rates with rate type <RateType>
    Then User receives status code as expected <StatusCode>
    Examples:
      | RateType | StatusCode |
      | TBS      | 400        |
      | GOOD     | 400        |

  @VATRates-400
  Scenario Outline: Verifying VAT rates with invalid apikey
    When the user send get request for VAT rates with rate type <RateType> and invalid apikey <apikey>
    Then User receives status code as expected <StatusCode>
    Examples:
      | RateType | apikey  | StatusCode |
      | TBS      | invalid | 403        |
      | GOOD     | invalid | 403        |

