Feature:Validate currency conversion

  @CurrencyConversion-200
  Scenario Outline: : Verifying currency conversion with valid fields
    When the user send a GET request with currencyfrom <currencyfrom>, amount <Amount> and currencyto <currencyto>
    Then the user verifies status code as <StatusCode> for currency conversion
    And Verify the fields as not null in response
    Examples:
      | currencyfrom | Amount | currencyto | StatusCode |
      | USD          | 15     | GBP        | 200        |

  @CurrencyConversion-400
  Scenario Outline: : Verifying currency conversion with invalid fields
    When the user send a GET request with currencyfrom <currencyfrom>, amount <Amount> and currencyto <currencyto>
    Then the user verifies status code as <StatusCode> for currency conversion
    Examples:
      | currencyfrom | Amount | currencyto | StatusCode |
      | US1          | 15     | GBP        | 400        |
      | USD          | 15     | GBS        | 400        |
      | 123          | 15     | GBP        | 400        |

  @CurrencyConversion-403
  Scenario Outline: Verifying currency conversion with invalid api key
    When the user send a GET request with currencyfrom <currencyfrom>, amount <Amount> currencyto <currencyto> with invalid apikey <apikey>
    Then the user verifies status code as <StatusCode> for currency conversion
    Examples:
      | currencyfrom | Amount | currencyto | apikey  | StatusCode |
      | USD          | 15     | GBP        | invalid | 403        |
