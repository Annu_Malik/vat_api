## Test Automation using java Serenity, cucumber and  maven

Open API - VatApi test cases has been created for LeasePlan, as test assignment.

## Getting Started

### Testing VAT Api

This project was done using Java Serenity, Cucumber BDD framework and Maven build tool.

### Installing

In order to execute API tests, it is necessary to configure CI/CD pipeline with docker maven image.

### How to build

#### Run below maven command to build the project.

$ mvn clean

### How to execute test cases [as below]

#### Run all the test cases.

$ mvn verify

### Write new test cases in a file with the path specified below

- src/test/resources/features  [for writing scenerio's]
- src/test/java/com.vatapi/steps  [for calling methods]
- src/test/java/com.vatapi/actions [for writing methods]
- src/test/java/com.vatapi/utils  [for writing util classes]
- src/test/java/com.vatapi/models [for writing models]

### Serenity HTML Reports

[Report will be generated in target/site/serenity/index.html file]

## Summary - Features Coverability

- Validate a VAT number
- Validate converting price to VAT price
- Validate creating, retrieving and deleting invoice
- Validate currency conversion
- Validate retrieving the current and updated currency conversion exchange rates
- Validate retrieving the current VAT rates for specified product type
- Validate the VAT rate check for specified country code and product type

## Created CI/CD Pipeline and generated html report

- Committed code on gitlab using commands: git clone, git commit -m "commit message", git add <filename>, git status,
  git push, git pull
- Created gitlab-ci.yml file for generating CI/CD pipeline.
- CI/CD pipeline will get trigger automatically after the commit and have made two stages in pipeline : build and test (
  this stage will create public folder and moves the reports inside it)

### See serenity HTML reports

- Goto CI/CD - pipelines -> stage test (click) -> report-job (click) -> click Browse (right side) -> Public (click) ->
  search file index.html (click and we can see reports), else we can also download the reports and see (that will
  download public folder, so we can search index.html inside it)

## Interactions of endpoints

### Validate a VAT number

- Endpoint -> https://eu.vatapi.com/v2/vat-number-check
- Action -> GET
- Description -> When we will send a get request with vatid of the company then verify status code as 200
- Request Input parameter -> vatid = LU2637
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","country_code": "LU","vat_number": 2637,"requester_country_code":
  null,"requester_vat_number": null,"validation": {"status": "ok","fault_string": null,"valid": false,"request_date": "
  2021-10-01+02:00","consultation_number": null,"consultation_authority": "EU VIES","company_name": "---","
  company_address": "---"} }


- Endpoint -> https://eu.vatapi.com/v2/vat-number-check
- Action -> GET
- Description -> When we will send a get request with invalid vatid of the company then we will get an error message,
  and we will verify the status code as 400
- Request Input parameter -> vatid = LU26375245
- Response Status Code -> 400
- Response -> {"status": 400,"message": "A max of 16 alphanumeric characters are allowed, check the length and ensure
  there are no spaces or other disallowed characters in the 'vatid' value." }


- Endpoint -> https://eu.vatapi.com/v2/vat-number-check
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the Missing Authentication Token
  error message, and we will verify the status code as 403
- Request Input parameter -> vatid = LU26375245, apikey=invalid
- Response Status Code -> 403
- Response -> {"message": "Missing Authentication Token"}


- Endpoint -> https://eu.vatapi.com/v2/vat-number-check
- Action -> GET
- Description -> When we will send a get request with valid vatid of the company then we will get the vat number details
  of the company, and we will verify if valid field is true then this is a valid vat number with 200 status code
- Request Input parameter -> vatid = LU26375245
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","country_code": "LU","vat_number": 26375245,"requester_country_code":
  null,"requester_vat_number": null,"validation": {"status": "ok","fault_string": null,"valid": true,"request_date": "
  2021-10-01+02:00","consultation_number": null,"consultation_authority": "EU VIES","company_name": "AMAZON EUROPE CORE
  S.A R.L.","company_address": "38, AVENUE JOHN F. KENNEDY\nL-1855 LUXEMBOURG"} }

### Validate converting price to VAT price

- Endpoint -> https://eu.vatapi.com/v2/vat-price
- Action -> GET
- Description -> When we will send a get request with all valid input parameters (country code,price,
  rate_type,rate_category,rate_level) then it will calculate and give the relevant VAT totals
- Request Input parameter -> country_code=IT, price=9, rate_type=TBE, rate_category=electronic, rate_level=reduced_1,
  vat_charging_method=inclusive
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","country_code": "IT","country": "Italy","rate_type": "TBE","
  rate_category": "electronic","rate_level": "reduced_1","vat_charging_method": "inclusive","vat_rate": 4,"price": 9,"
  price_excl_vat": 8.65,"price_incl_vat": 9,"vat_total": 0.35 }


- Endpoint -> https://eu.vatapi.com/v2/vat-price
- Action -> GET
- Description -> When we will send a get request with invalid input parameter then we will get an error message.
- Request Input parameter -> country_code=TI, price=9, rate_type=TBE, rate_category=electronic, rate_level=reduced_1,
  vat_charging_method=inclusive
- Response Status Code -> 400
- Response -> {"status": 400,"message": "The country code 'TI' is outside the scope of EU VAT."}


- Endpoint -> https://eu.vatapi.com/v2/vat-price
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the Missing Authentication Token
  error message
- Request Input parameter -> country_code=IT, price=9, rate_type=TBE, rate_category=electronic, rate_level=reduced_1,
  vat_charging_method=inclusive, apikey=invalid
- Response Status Code -> 403
- Response -> {"message": "Missing Authentication Token"}

### Validate creating, retrieving and deleting invoice

######## Create invoice

- Endpoint -> https://eu.vatapi.com/v2/invoice
- Action -> POST
- Description -> When we will send a post request with all valid input parameters then it will create the invoice.
- Request Input parameter -> {"settings_identifier": "12a048d4094337e07efd0f1fd9fe6521","type": "sale","currency": "EUR"
  ,"customer": {"name": "Amazon"},"
  line_items": [{"description": "Product 1","quantity": 1,"price_each": 10.00,"vat_rate": 17 }]}
- Response Status Code -> 201
- Response -> {"status": 201,"message": "success","invoice_id": "ad13a2a07c","invoice_number": 193,"order_ref": null,"
  invoice_url": "https://vatinvoice.co.uk/ad13a2a07c","created": "2021-10-01 08:00:01","updated": "2021-10-01 08:00:01"
  ,"issue_date": "2021-10-01 08:00:01","tax_point": "2021-10-01 08:00:01","type": "sale","currency": "EUR","
  local_currency": {"to": null,"rate": null },"order_discount_method": "fixed","order_discount_amount": null,"
  vat_charging_method": "exclusive","zero_rated": false,"business": {"trading_name": "VAT API","trading_address": "
  International House, 24 Holborn Viaduct, City of London, London, EC1A 2BN","vat_number": "GB289111588","
  logo": "https://vatapi.com/assets/images/logo-invoice.jpg"},"customer": {"name": "Amazon","address": null,"
  vat_number": null },"
  line_items": [{"id": "9d752cb08e","sku": null,"description": "Product1","quantity": 1,"price_each": null,"discount": null,"vat_rate": null,"line_total": 0 } ]
  ,"totals": {"line_items_discount": 0,"subtotal": 0,"discount": null,"vat": 0,"vat_local": null,"total": 0 },"notes":
  null }


- Endpoint -> https://eu.vatapi.com/v2/invoice
- Action -> POST
- Description -> When we will send a post request with invalid input parameter then it will show error.
- Request Input parameter -> {"settings_identifier": "12a048d4094337e07efd0f1fd9fe6521","type": "sal","currency": "EUR"
  ,"customer": {"name": "Amazon"},"
  line_items": [{"description": "Product 1","quantity": 1,"price_each": 10.00,"vat_rate": 17 }]}
- Response Status Code -> 400
- Response -> {"status": 400,"message": "The invoice 'type' value must be 'sale' or 'refund'."}


- Endpoint -> https://eu.vatapi.com/v2/invoice
- Action -> POST
- Description -> When we will send a post request with invalid apikey then status code will be 403.
- Request Input parameter -> {"settings_identifier": "12a048d4094337e07efd0f1fd9fe6521","type": "sale","currency": "EUR"
  ,"customer": {"name": "Amazon"},"
  line_items": [{"description": "Product 1","quantity": 1,"price_each": 10.00,"vat_rate": 17 }]},apikey=invalid
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}

######## Delete invoice

- Endpoint -> https://eu.vatapi.com/v2/invoice/{{invoice_id}}
- Action -> DELETE
- Description -> When we will send a delete request with valid invoice_id then it will delete the invoice. Invoice was created earlier in same test case
- Request Input parameter -> invoice_id
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","invoice_id": "3fe94a0023"}


- Endpoint -> https://eu.vatapi.com/v2/invoice
- Action -> DELETE
- Description -> When we will send a delete request with invalid invoice_id then we will see could not found error with status code 404
  message.
- Request Input parameter -> invoice_id=null
- Response Status Code -> 404
- Response -> {"status": 404,"message": "Could not find an invoice for the given invoice id."}


- Endpoint -> https://eu.vatapi.com/v2/invoice/{{invoice_id}}
- Action -> DELETE
- Description -> When we will send a delete request with invalid apikey then status code will be 403.
- Request Input parameter -> invoice_id=null
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}

######## Retrieve invoice

- Endpoint -> https://eu.vatapi.com/v2/invoice/{{invoice_id}}
- Action -> GET
- Description -> When we will send a get request with valid invoice_id then it will retrieve the invoice details.
- Request Input parameter -> invoice_id
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","invoice_id": "432aca3a1e","invoice_number": 196,"order_ref": null,"
  invoice_url": "https://vatinvoice.co.uk/432aca3a1e","created": "2021-10-01 08:00:07","updated": "2021-10-01 08:00:07"
  ,"issue_date": "2021-10-01 08:00:07","tax_point": "2021-10-01 08:00:07","type": "sale","currency": "EUR","
  local_currency": {"to": null,"rate": null},"order_discount_method": "fixed","order_discount_amount": null,"
  vat_charging_method": "exclusive","zero_rated": false,"business": {"trading_name": "VAT API","trading_address": "
  International House, 24 Holborn Viaduct, City of London, London, EC1A 2BN","vat_number": "GB289111588","
  logo": "https://vatapi.com/assets/images/logo-invoice.jpg"},"customer": {"name": "Amazon","address": null,"
  vat_number": null },"
  line_items": [{"id": "7ba6d33c37","sku": null,"description": "Product1","quantity": 1,"price_each": null,"discount": null,"vat_rate": null,"line_total": 0 }]
  ,"totals": {"line_items_discount": 0,"subtotal": 0,"discount": null,"vat": 0,"vat_local": null,"total": 0 },"notes":
  null }


- Endpoint -> https://eu.vatapi.com/v2/invoice
- Action -> GET
- Description -> When we will send a get request with invalid invoice_id then we will see could not found error message.
- Request Input parameter -> invoice_id=null
- Response Status Code -> 404
- Response -> {"status": 404,"message": "Could not find an invoice for the given invoice id."}

### Validate currency conversion

- Endpoint -> https://eu.vatapi.com/v2/currency-conversion
- Action -> GET
- Description -> When we will send a get request with all valid input parameters then it will convert the price as per
  requirement and give all the details, and we will verify that the response fields are not null.
- Request Input parameter -> currency_from=USD, amount=15, currency_to=GBP
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","currency_from": "USD","currency_to": "GBP","amount_from": 15,"
  amount_to": 10.99,"rate": 1.3646,"rate_last_updated": "2021-10-01 05:00:56"}


- Endpoint -> https://eu.vatapi.com/v2/currency-conversion
- Action -> GET
- Description -> When we will send a get request with invalid input parameter then we will get an error message.
- Request Input parameter -> currency_from=US1, amount=15, currency_to=GBP
- Response Status Code -> 400
- Response -> {"status": 400,"message": "Please provide a valid currency code to convert from."}


- Endpoint -> https://eu.vatapi.com/v2/currency-conversion
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the Invalid API Key error message
- Request Input parameter -> currency_from=US1, amount=15, currency_to=GBP, apikey=invalid
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}

### Validate retrieving the current and updated currency conversion exchange rates

- Endpoint -> https://eu.vatapi.com/v2/currency-conversion-rates
- Action -> GET
- Description -> When we will send a get request with currency_code then it will give the current and updated currency
  conversion exchange rates
- Request Input parameter -> currency_code=EUR
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","reference_rates": "EUR","rates_last_updated": "2021-10-01 00:20:32","
  rates": [{"country_code": "USD","rate": 1.1579 },{"country_code": "JPY","rate": 129.67 },{"country_code": "BGN","rate": 1.9558 },{"country_code": "CZK","rate": 25.495 }, {"country_code": "DKK","rate": 7.436},{"country_code": "GBP","rate": 0.86053 }, {"country_code": "HUF","rate": 360.19},{"country_code": "PLN","rate": 4.6197 },{"country_code": "RON","rate": 4.9475}, {"country_code": "SEK","rate": 10.1683},{"country_code": "CHF","rate": 1.083},{"country_code": "ISK","rate": 150.9},{"country_code": "NOK","rate": 10.165},{"country_code": "HRK","rate": 7.4889 },{"country_code": "RUB","rate": 84.3391},{"country_code": "TRY","rate": 10.2981},{"country_code": "AUD","rate": 1.6095 },{"country_code": "BRL","rate": 6.2631 },{"country_code": "CAD","rate": 1.475 },{"country_code": "CNY","rate": 7.4847 },{"country_code": "HKD","rate": 9.0184},{"country_code": "IDR","rate": 16572},{"country_code": "ILS","rate": 3.7363},{"country_code": "INR","rate": 86.0766},{"country_code": "KRW","rate": 1371.58 },{"country_code": "MXN","rate": 23.7439},{"country_code": "MYR","rate": 4.8475 },{"country_code": "NZD","rate": 1.6858 },{"country_code": "PHP","rate": 59.066 },{"country_code": "SGD","rate": 1.576 },{"country_code": "THB","rate": 39.235 },{"country_code": "ZAR","rate": 17.5629 }]
  }


- Endpoint -> https://eu.vatapi.com/v2/currency-conversion-rates
- Action -> GET
- Description -> When we will send a get request with invalid input parameter then we will get an error message.
- Request Input parameter -> currency_code=EUT
- Response Status Code -> 400
- Response -> {"status": 400,"message": "Please provide a valid currency code to retrieve conversion rates."}


- Endpoint -> https://eu.vatapi.com/v2/currency-conversion-rates
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the Invalid API Key error message
- Request Input parameter -> currency_code=EUR apikey=invalid
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}

### Validate retrieving the current VAT rates for specified product type.

- Endpoint -> https://eu.vatapi.com/v2/vat-rates
- Action -> GET
- Description -> When we will send a get request with rate_type then it will give the current VAT rates for the
  specified product type
- Request Input parameter -> rate_type=TBE
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","rate_type": "TBE","countries": {"AT": {"country": "Austria","rates":
  {"telecoms": {"standard": {"rate": 20,"comments": null } },"broadcasting": {"standard": {"rate": 10,"comments": null
  }},"electronic": {"standard": {"rate": 20,"comments": null },"reduced_1": {"rate": 10,"comments": "eBooks"} } } },"
  BE": {"country": "Belgium","rates": {"telecoms": {"standard": {"rate": 21,"comments": null } },"broadcasting": {"
  standard": {"rate": 21,"comments": null } },"electronic": {"standard": {"rate": 21,"comments": null },"reduced_1": {"
  rate": 6,"comments": "eBooks"} } } },"BG": {"country": "Bulgaria","rates": {"telecoms": {"standard": {"rate": 20,"
  comments": null } },"broadcasting": {"standard": {"rate": 20,"comments": null } },"electronic": {"standard": {"rate":
  20,"comments": null },"reduced_1": {"rate": 9,"comments": "eBooks"} } } },"HR": {"country": "Croatia","rates": {"
  telecoms": {"standard": {"rate": 25,"comments": null } },"broadcasting": {"standard": {"rate": 25,"comments": null }
  },"electronic": {"standard": {"rate": 25,"comments": null },"reduced_1": {"rate": 5,"comments": "eBooks"} } } },"CY":
  {"country": "Cyprus","rates": {"telecoms": {"standard": {"rate": 19,"comments": null } },"broadcasting": {"standard":
  {"rate": 19,"comments": null } },"electronic": {"standard": {"rate": 19,"comments": null } } } },"CZ": {"country": "
  Czech Republic","rates": {"telecoms": {"standard": {"rate": 21,"comments": null } },"broadcasting": {"standard": {"
  rate": 21,"comments": null } },"electronic": {"standard": {"rate": 21,"comments": null },"reduced_1": {"rate": 10,"
  comments": "eBooks"} } } },"DK": {"country": "Denmark","rates": {"telecoms": {"standard": {"rate": 25,"comments": null
  } },"broadcasting": {"standard": {"rate": 25,"comments": null } },"electronic": {"standard": {"rate": 25,"comments":
  null } } } },"EE": {"country": "Estonia","rates": {"telecoms": {"standard": {"rate": 20,"comments": null } },"
  broadcasting": {"standard": {"rate": 20,"comments": null } },"electronic": {"standard": {"rate": 20,"comments": null
  },"reduced_1": {"rate": 9,"comments": "eBooks"}}}},"FI": {"country": "Finland","rates": {"telecoms":{"standard": {"
  rate": 24,"comments": null } },"broadcasting": {"standard": {"rate": 24,"comments": null } },"electronic": {"
  standard": {"rate": 24,"comments": null },"reduced_1": {"rate": 10,"comments": "eBooks"} } } },"FR": {"country": "
  France","rates": {"telecoms": {"standard": {"rate": 20,"comments": null } },"broadcasting": {"standard": {"rate": 20,"
  comments": "Offre triple play"},"reduced_1": {"rate": 10,"comments": "Offres TV seule"} },"electronic": {"standard":
  {"rate": 20,"comments": null },"reduced_1": {"rate": 5.5,"comments": "eBooks"},"reduced_2": {"rate": 2.1,"comments": "
  eNewspapers"}}}},"DE": {"country": "Germany","rates": {"telecoms": {"standard": {"rate": 19,"comments": null}},"
  broadcasting": {"standard": {"rate": 19,"comments": null } },"electronic": {"standard": {"rate": 19,"comments": null
  },"reduced_1": {"rate": 7,"comments": "eBooks"}}}},"GR": {"country": "Greece","rates": {"telecoms": {"standard": {"
  rate": 24,"comments": null } },"broadcasting": {"standard": {"rate": 24,"comments": null } },"electronic": {"
  standard": {"rate": 24,"comments": null } } } },"HU": {"country": "Hungary","rates": {"telecoms": {"standard": {"
  rate": 27,"comments": null },"reduced_1": {"rate": 5,"
  comments": "Only for Internet access service as defined in point 2 of Article 2 of 2015/2120/EU."}},"broadcasting": {"
  standard": {"rate": 27,"comments": null }},"electronic": {"standard": {"rate": 27,"comments": null }}}},"IE": {"
  country": "Ireland","rates": {"telecoms": {"standard": {"rate": 23,"comments": null }},"broadcasting": {"standard": {"
  rate": 23,"comments": null }},"electronic": {"standard": {"rate": 23,"comments": null },"reduced_1": {"rate": 9,"
  comments": "eBooks"}}}},"IT": {"country": "Italy","rates": {"telecoms": {"standard": {"rate": 22,"comments": null }},"
  broadcasting": {"standard": {"rate": 22,"comments": null }},"electronic": {"standard": {"rate": 22,"comments": null
  },"reduced_1": {"rate": 4,"comments": "eBooks"}}}},"LV": {"country": "Latvia","rates": {"telecoms": {"standard": {"
  rate": 21,"comments": null }},"broadcasting": {"standard": {"rate": 21,"comments": null }},"electronic": {"standard":
  {"rate": 21,"comments": null }}}},"LT": {"country": "Lithuania","rates": {"telecoms": {"standard": {"rate": 21,"
  comments": null }},"broadcasting": {"standard": {"rate": 21,"comments": null }},"electronic": {"standard": {"rate":
  21,"comments": null },"reduced_1": {"rate": 5,"comments": "eBooks"}}}},"LU": {"country": "Luxembourg","rates": {"
  telecoms": {"standard": {"rate": 17,"comments": null }},"broadcasting": {"standard": {"rate": 17,"comments": "
  Reception of radio and TV broadcasting services whose content is exclusively for adults, regardless of the electronic
  communications network used"},"reduced_1": {"rate": 3,"comments": null } },"electronic": {"standard": {"rate": 17,"
  comments": null },"reduced_1": {"rate": 3,"comments": "eBooks"}}}},"MT": {"country": "Malta","rates": {"telecoms": {"
  standard": {"rate": 18,"comments": null}},"broadcasting": {"standard": {"rate": 18,"comments": null }},"electronic":
  {"standard": {"rate": 18,"comments": null },"reduced_1": {"rate": 5,"comments": "eBooks"}}}},"NL": {"country": "
  Netherlands","rates": {"telecoms": {"standard": {"rate": 21,"comments": null } },"broadcasting": {"standard": {"rate":
  21,"comments": null }},"electronic": {"standard": {"rate": 21,"comments": null },"reduced_1": {"rate": 9,"comments": "
  eBooks"} } } },"PL": {"country": "Poland","rates": {"telecoms": {"standard": {"rate": 23,"comments": null }},"
  broadcasting": {"standard": {"rate": 23,"comments": "Services related to rental of audio and video content on-demand"
  },"reduced_1": {"rate": 8,"comments": null }},"electronic": {"standard": {"rate": 23,"comments": null },"reduced_1":
  {"rate": 7,"comments": "eNewspapers"},"reduced_2": {"rate": 5,"comments": "eBooks"}}}},"PT": {"country": "Portugal","
  rates": {"telecoms": {"standard": {"rate": 23,"comments": null }},"broadcasting": {"standard": {"rate": 23,"comments":
  null }},"electronic": {"standard": {"rate": 23,"comments": null },"reduced_1": {"rate": 6,"comments": "eBooks"}}}},"
  RO": {"country": "Romania","rates": {"telecoms": {"standard": {"rate": 19,"comments": null}},"broadcasting": {"
  standard": {"rate": 19,"comments": null}},"electronic": {"standard": {"rate": 19,"comments": null}}}},"SK": {"
  country": "Slovakia","rates": {"telecoms": {"standard": {"rate": 20,"comments": null }},"broadcasting": {"standard":
  {"rate": 20,"comments": null}},"electronic": {"standard": {"rate": 20,"comments": null}}}},"SI": {"country": "
  Slovenia","rates": {"telecoms": {"standard": {"rate": 22,"comments": null }},"broadcasting": "standard": {"rate": 22,"
  comments": null } },"electronic": {"standard": {"rate": 22,"comments": null },"reduced_1": {"rate": 5,"comments": "
  eBooks"}}}},"ES": {"country": "Spain","rates": {"telecoms": {"standard": {"rate": 21,"comments": null } },"
  broadcasting": {"standard": {"rate": 21,"comments": null } },"electronic": {"standard": {"rate": 21,"comments": null
  },"reduced_1": {"rate": 4,"comments": "eBooks"} } } },"SE": {"country": "Sweden","rates": {"telecoms": {"standard": {"
  rate": 25,"comments": null }},"broadcasting": {"standard": {"rate": 25,"comments": null}},"electronic": {"standard":
  {"rate": 25,"comments": null},"reduced_1": {"rate": 6,"comments": "eBooks"}}}},"GB": {"country": "United Kingdom","
  rates": {"telecoms": {"standard": {"rate": 20,"comments": null }},"broadcasting": {"standard": {"rate": 20,"comments":
  null}},"electronic": {"standard": {"rate": 20,"comments": null}}}},"XI": {"country": "Northern Island","rates": {"
  telecoms": {"standard": {"rate": 20,"comments": null } },"broadcasting": {"standard": {"rate": 20,"comments": null
  }},"electronic": {"standard": {"rate": 20,"comments": null}}}}}}


- Endpoint -> https://eu.vatapi.com/v2/vat-rates
- Action -> GET
- Description -> When we will send a get request with invalid input parameter then we will get an error message.
- Request Input parameter -> rate_type=TBS
- Response Status Code -> 400
- Response -> {"status": 400,"message": "Please provide a valid rate type."}


- Endpoint -> https://eu.vatapi.com/v2/vat-rates
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the Invalid API Key error message
- Request Input parameter -> rate_type=TBE, apikey=invalid
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}

### Validate the VAT rate check for specified country code and product type

- Endpoint -> https://eu.vatapi.com/v2/vat-rate-check
- Action -> GET
- Description -> When we will send a get request with rate_type and country_code then if vat_applies field is true then
  it is a valid vat rate for the specified country_code and rate_type.
- Request Input parameter -> rate_type=TBE,country_code=NL
- Response Status Code -> 200
- Response -> {"status": 200,"message": "success","country_code": "NL","country": "Netherlands","vat_applies": true,"
  rate_type": "TBE","rates": {"telecoms": {"standard": {"rate": 21,"comments": null }},"broadcasting": {"standard": {"
  rate": 21,"comments": null }},"electronic": {"standard": {"rate": 21,"comments": null },"reduced_1": {"rate": 9,"
  comments": "eBooks"}}}}


- Endpoint -> https://eu.vatapi.com/v2/vat-rate-check
- Action -> GET
- Description -> When we will send a get request with invalid input parameter then we will get an error message.
- Request Input parameter -> rate_type=TB,country_code=NL
- Response Status Code -> 400
- Response -> {"status": 400,"message": "Please provide a valid rate type."}


- Endpoint -> https://eu.vatapi.com/v2/vat-rate-check
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the invalid API Key error message
- Request Input parameter -> rate_type=TBE,country_code=NL, apikey=invalid
- Response Status Code -> 403
- Response -> {"status": 403,"message": "Invalid API Key"}
